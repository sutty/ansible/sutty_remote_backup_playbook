Sutty Remote Backups Playbook
-----------------------------

Generates remote and encrypted backups using Restic.  It should work
with any host but was developed for shared hosting.

It tries not to expose secrets so they're only stored temporarily on
hosts while the tasks runs..

Host variables
--------------

`restic_directories`: which directories to backup, use "['/']" for
everything.

`mysql_user`: if set, dump databases before running.

`mysql_host`: if set, dump database from this hosts before running.

Credentials
-----------

Backup passwords are stored on `_credentials/HOST/restic_password`.
Edit `group_vars/sutty_remote_backup.yml` or add a `restic_password`
variable on each host if you don't want this behavior.

```
_credentials/
  HOST/
    restic_password # Backup encryption key, auto-generated as 128 chars password
    mysql_pass # MySQL pass, you can create this file after configuring in the server
  b2_account_id # Backblaze B2
  b2_account_key
  b2_bucket
```
